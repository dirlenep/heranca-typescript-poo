import { Endereco } from "./endereco";
import { PessoaFisica } from "./pessoaFisica";
import { PessoaJuridica } from "./pessoaJuridica";

let endereco = new Endereco("Santa Catarina", "Blumenau", "Centro", "7 de setembre", 1232, "89108-099"); 
let pessoaFisica = new PessoaFisica("000.000.000-12", "0.000.000", "2021.01.01", "Fulano", endereco, "(47)9999-9999");
let empresa = new PessoaJuridica("Proway", endereco, "(47)3030-0000", "01.000.000/0001-00", "12345678", "2000/05/20");

console.table(pessoaFisica);
console.table(empresa);
