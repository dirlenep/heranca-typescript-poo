"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PessoaJuridica = void 0;
var pessoa_1 = require("./pessoa");
var PessoaJuridica = /** @class */ (function (_super) {
    __extends(PessoaJuridica, _super);
    function PessoaJuridica(nomeEmpresa, enderecoEmpresa, telefone, cnpj, inscricaoEstadual, dataFundacao) {
        var _this = _super.call(this, nomeEmpresa, enderecoEmpresa, telefone) || this;
        _this.cnpj = cnpj;
        _this.inscricaoEstadual = inscricaoEstadual;
        _this.dataFundacao = dataFundacao;
        return _this;
    }
    Object.defineProperty(PessoaJuridica.prototype, "cnpj", {
        get: function () {
            return this._cnpj;
        },
        set: function (value) {
            this._cnpj = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PessoaJuridica.prototype, "inscricaoEstadual", {
        get: function () {
            return this._inscricaoEstadual;
        },
        set: function (value) {
            this._inscricaoEstadual = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PessoaJuridica.prototype, "dataFundacao", {
        get: function () {
            return this._dataFundacao;
        },
        set: function (value) {
            this._dataFundacao = value;
        },
        enumerable: false,
        configurable: true
    });
    return PessoaJuridica;
}(pessoa_1.Pessoa));
exports.PessoaJuridica = PessoaJuridica;
