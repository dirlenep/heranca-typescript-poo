"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pessoa = void 0;
var Pessoa = /** @class */ (function () {
    //Método Construtor 
    function Pessoa(nome, endereco, telefone) {
        this.nome = nome;
        this.endereco = endereco;
        this.telefone = telefone;
    }
    Object.defineProperty(Pessoa.prototype, "endereco", {
        //Método Assessor 
        get: function () {
            return this._endereco;
        },
        //Método Mutattor 
        set: function (value) {
            this._endereco = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "nome", {
        get: function () {
            return this._nome;
        },
        set: function (value) {
            this._nome = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "telefone", {
        get: function () {
            return this._telefone;
        },
        set: function (value) {
            this._telefone = value;
        },
        enumerable: false,
        configurable: true
    });
    //Método Worker
    Pessoa.prototype.calculaIdade = function () {
        return 15;
    };
    return Pessoa;
}());
exports.Pessoa = Pessoa;
