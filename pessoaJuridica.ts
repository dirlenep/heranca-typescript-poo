import { Pessoa } from "./pessoa";
import { Endereco } from "./endereco";

export class PessoaJuridica extends Pessoa{
    
    private _cnpj: string;
    private _inscricaoEstadual: string; 
    private _dataFundacao: string;  

    constructor(nomeEmpresa: string, enderecoEmpresa: Endereco, telefone: string, cnpj: string, inscricaoEstadual: string, dataFundacao: string) {
        super(nomeEmpresa, enderecoEmpresa, telefone);
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.dataFundacao = dataFundacao;
    }

    public get cnpj(): string {
        return this._cnpj;
    }
    public set cnpj(value: string) {
        this._cnpj = value;
    }

    public get inscricaoEstadual(): string {
        return this._inscricaoEstadual;
    }
    public set inscricaoEstadual(value: string) {
        this._inscricaoEstadual = value;
    }

    public get dataFundacao(): string {
        return this._dataFundacao;
    }
    public set dataFundacao(value: string) {
        this._dataFundacao = value;
    }
}